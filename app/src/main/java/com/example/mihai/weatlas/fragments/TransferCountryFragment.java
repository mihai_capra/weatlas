package com.example.mihai.weatlas.fragments;


import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mihai.weatlas.adapters.TransferAdapter;
import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.loaders.CountryTransferLoader;
import com.example.mihai.weatlas.models.Item;
import com.example.mihai.weatlas.R;

import java.util.List;


public class TransferCountryFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Item>> {

    private RecyclerView recyclerView;
    private TransferAdapter transferAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_transfer_country, container, false);

        recyclerView = view.findViewById(R.id.country_transfer);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(256);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        LoaderManager loaderManager = getActivity().getLoaderManager();
        loaderManager.initLoader(6, null, this);

        return view;
    }

    @Override
    public Loader<List<Item>> onCreateLoader(int i, Bundle bundle) {
        return new CountryTransferLoader(getContext(), Common.COUNTRY_TRANSFER);
    }


    @Override
    public void onLoadFinished(Loader<List<Item>> loader, List<Item> itemList) {
        if(itemList != null && !itemList.isEmpty()){
            transferAdapter = new TransferAdapter(itemList);
            recyclerView.setAdapter(transferAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Item>> loader) {

    }
}
