package com.example.mihai.weatlas.models;


public class CityOffer {

    private int id;
    private String offerName;
    private String imageLink;
    private String goType;
    private String offerPrice;
    private String offerPriceFor;
    private String rating;

    public CityOffer(int id, String offerName, String imageLink, String goType,
                     String offerPrice, String offerPriceFor, String rating) {
        this.id = id;
        this.offerName = offerName;
        this.imageLink = imageLink;
        this.goType = goType;
        this.offerPrice = offerPrice;
        this.offerPriceFor = offerPriceFor;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public String getOfferName() {
        return offerName;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getGoType() {
        return goType;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public String getOfferPriceFor() {
        return offerPriceFor;
    }

    public String getRating() {
        return rating;
    }
}
