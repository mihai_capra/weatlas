package com.example.mihai.weatlas.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.mihai.weatlas.R;
import com.example.mihai.weatlas.adapters.CityAdapter;
import com.example.mihai.weatlas.fragments.CityFragment;
import com.example.mihai.weatlas.fragments.TripsFragment;

public class CountryCityOffers extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_city_offers);

        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.city_fragment_id, new CityFragment())
                    .commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_search:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Item Search");
                final EditText input = new EditText(this);
                alert.setView(input);
                //VIE
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        CityFragment cityFragment = (CityFragment) getSupportFragmentManager().
                                findFragmentById(R.id.city_fragment_id);

                        if(cityFragment != null) {
                            cityFragment.findCity(input.getText().toString());
                        }
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        return;
                    }
                });
                alert.show();
                return true;

            case R.id.action_settings:

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
