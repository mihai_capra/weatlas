package com.example.mihai.weatlas.network;


import android.text.TextUtils;

import com.example.mihai.weatlas.common.StringConstants;
import com.example.mihai.weatlas.models.CityOffer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class GetCityOffers {
    public static List<CityOffer> getJSONList(String response){

        if(TextUtils.isEmpty(response)){
            return null;
        }

        List<CityOffer> items = new ArrayList<>();

        try{
            JSONObject baseJSON = new JSONObject(response);
            JSONArray values = baseJSON.getJSONArray("activity");
            for(int i = 0; i < values.length(); i++){
                JSONObject jsonItem = values.getJSONObject(i);

                //get photo link or get default image
                String imageLink = jsonItem.getJSONObject("photos")
                        .getJSONArray("photo").getJSONObject(0).getString("medium");

                //offer price for group/individual
                String offerPriceFor = (jsonItem.getString("activityOrderType").equals("Individual")) ?
                        StringConstants.price_for_individual_RU :
                        StringConstants.price_for_group_RU;

                //offer name
                String offerName = jsonItem.getString("name");


                //offer go type plus time
                String goType = jsonItem.getString("goType");
                goType += ", " + jsonItem.getString("duration");

                //price

                String offerPrice = jsonItem.getString("Price");
                offerPrice += " " + jsonItem.getString("Currency");

                DecimalFormat df = new DecimalFormat("0.0");
                String angleFormated = df.format(jsonItem.getDouble("ActivityRate"));

                items.add(new CityOffer(
                        jsonItem.getInt("id"),
                        offerName,
                        imageLink,
                        goType,
                        offerPrice,
                        offerPriceFor,
                        angleFormated
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return items;
    }

    public static List<CityOffer> getCityOffersList(String stringURL){
        URL url = NetworkUtil.createURL(stringURL);

        String resultJSON = null;
        try{
            resultJSON = NetworkUtil.makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return getJSONList(resultJSON);
    }
}
