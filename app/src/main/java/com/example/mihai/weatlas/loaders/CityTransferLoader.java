package com.example.mihai.weatlas.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.mihai.weatlas.models.Item;
import com.example.mihai.weatlas.network.GetCityTransfer;

import java.util.List;

public class CityTransferLoader extends AsyncTaskLoader<List<Item>> {
    private String stringURL;

    public CityTransferLoader(Context context, String stringURL) {
        super(context);
        this.stringURL = stringURL;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<Item> loadInBackground() {
        return GetCityTransfer.getCountryTransferList(stringURL);
    }
}
