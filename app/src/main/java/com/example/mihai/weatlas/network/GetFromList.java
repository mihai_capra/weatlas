package com.example.mihai.weatlas.network;

import android.text.TextUtils;

import com.example.mihai.weatlas.models.GetFrom;
import com.example.mihai.weatlas.Utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class GetFromList {

    public static List<GetFrom> ExtractData(String response){

        if(TextUtils.isEmpty(response)){
            return null;
        }
        List<GetFrom> values = new ArrayList<>();
        try{
            JSONObject baseJSON = new JSONObject(response);
            JSONArray  jsonArray = baseJSON.getJSONArray("place");
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                values.add(new GetFrom(jsonItem.getInt("fromId"), jsonItem.getString("value")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return values;
    }


    public static List<GetFrom> FetchGetFromList(String stringURL){
        URL url = NetworkUtil.createURL(stringURL);

        String resultJSON = null;
        try{
            resultJSON = NetworkUtil.makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Extract Data

        return ExtractData(resultJSON);
    }
}
