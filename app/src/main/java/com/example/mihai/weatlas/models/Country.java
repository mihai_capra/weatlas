package com.example.mihai.weatlas.models;

/**
 * Created by mihai on 9/15/2017.
 */

public class Country {

    private int id;
    private String countryName;
    private String activitiesNumber;
    private String imageLink;
    private String IATACountry;

    public Country(int id, String countryName, String activitiesNumber, String imageLink, String IATACountry) {
        this.id = id;
        this.countryName = countryName;
        this.activitiesNumber = activitiesNumber;
        this.imageLink = imageLink;
        this.IATACountry = IATACountry;
    }


    public int getId() {
        return id;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getActivitiesNumber() {
        return activitiesNumber;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getIATACountry() {
        return IATACountry;
    }
}
