package com.example.mihai.weatlas.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.mihai.weatlas.models.Item;
import com.example.mihai.weatlas.network.GetTransferCountries;

import java.util.List;


public class CountryTransferLoader extends AsyncTaskLoader<List<Item>> {

    private String stringURL;

    public CountryTransferLoader(Context context, String stringURL) {
        super(context);
        this.stringURL = stringURL;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        onForceLoad();
    }

    @Override
    public List<Item> loadInBackground() {
        return GetTransferCountries.getCountryTransferList(stringURL);
    }
}
