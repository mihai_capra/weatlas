package com.example.mihai.weatlas.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.mihai.weatlas.models.Country;
import com.example.mihai.weatlas.network.GetTripOffers;

import java.util.List;


public class TripsLoader extends AsyncTaskLoader<List<Country>> {

    private String stringURL;

    public TripsLoader(Context context, String stringURL) {
        super(context);
        this.stringURL = stringURL;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<Country> loadInBackground() {
        return GetTripOffers.getCountryList(stringURL);
    }
}
