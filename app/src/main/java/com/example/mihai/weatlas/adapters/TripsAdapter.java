package com.example.mihai.weatlas.adapters;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mihai.weatlas.activities.CountryCityOffers;
import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.models.Country;
import com.example.mihai.weatlas.R;

import java.util.List;


public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    private List<Country> items;

    public TripsAdapter(List<Country> items){
        this.items = items;
    }

    @Override
    public TripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TripsAdapter.ViewHolder holder, final int position) {
        holder.countryImage.setImageURI(Uri.parse(items.get(position).getImageLink()));
        holder.countryName.setText(items.get(position).getCountryName());
        holder.countryActivities.setText(items.get(position).getActivitiesNumber());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, CountryCityOffers.class);
                intent.putExtra(Common.COUNTRY_ID, items.get(holder.getAdapterPosition()).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardItem;
        TextView countryName;
        TextView countryActivities;
        ImageView countryImage;

        public ViewHolder(final View itemView) {
            super(itemView);

            cardItem = itemView.findViewById(R.id.card_view_item);
            countryImage = itemView.findViewById(R.id.image_view_item);
            countryName = itemView.findViewById(R.id.name_txt_item);
            countryActivities = itemView.findViewById(R.id.offers_number_txt);
        }

    }

    public Country getItem(int position){
        return items.get(position);
    }

}
