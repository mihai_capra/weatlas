package com.example.mihai.weatlas.fragments;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mihai.weatlas.adapters.TripsAdapter;
import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.loaders.TripsLoader;
import com.example.mihai.weatlas.models.Country;
import com.example.mihai.weatlas.R;

import java.util.ArrayList;
import java.util.List;

public class TripsFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<List<Country>>{

    private RecyclerView countryRecyclerView;
    private TripsAdapter tripsAdapter;
    private List<Country> itemsFound;
    private LoaderManager loaderManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.trip_fragment, container, false);
        countryRecyclerView = view.findViewById(R.id.recycler_view_main);

        loaderManager = getActivity().getLoaderManager();
        loaderManager.initLoader(Common.TRIP_OFFERS_LOADER_ID, null, this);

        return view;
    }

    @Override
    public Loader<List<Country>> onCreateLoader(int i, Bundle bundle) {
        return new TripsLoader(getContext(), Common.COUNTRY_LIST_OFFERS);
    }

    @Override
    public void onLoadFinished(Loader<List<Country>> loader, List<Country> countries) {
        if(countries != null && !countries.isEmpty()) {
            countryRecyclerView.setHasFixedSize(true);
            countryRecyclerView.setItemViewCacheSize(256);
            countryRecyclerView.setDrawingCacheEnabled(true);
            countryRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            countryRecyclerView.setItemAnimator(new DefaultItemAnimator());
            tripsAdapter = new TripsAdapter(countries);
            countryRecyclerView.setAdapter(tripsAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Country>> loader) {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void findCountry(String text){
        int size = countryRecyclerView.getAdapter().getItemCount();

         itemsFound = new ArrayList<>();

        for(int i=0; i<size; i++){
            Country item = ((TripsAdapter)countryRecyclerView.getAdapter()).getItem(i);
            if(item.getCountryName().contains(text)
                    || item.getCountryName().startsWith(text)){
                itemsFound.add(item);
            }
            else if(item.getIATACountry().contains(text) || item.getIATACountry().equals(text)){
                itemsFound.add(item);
            }
        }
        countryRecyclerView.setAdapter(new TripsAdapter(itemsFound));
    }
}
