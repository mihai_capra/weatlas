package com.example.mihai.weatlas.fragments;


import android.os.Bundle;
import android.app.LoaderManager;
import android.content.Loader;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mihai.weatlas.R;
import com.example.mihai.weatlas.adapters.CityOffersAdapter;
import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.loaders.CityOffersLoader;
import com.example.mihai.weatlas.models.CityOffer;

import java.util.List;

public class CityOffersFragment extends Fragment
    implements LoaderManager.LoaderCallbacks<List<CityOffer>>{

    private RecyclerView cityOffersRecycler;
    private CityOffersAdapter cityOffersAdapter;
    private int cityId;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.city_offers_fragment, container, false);

        cityId = getArguments().getInt("CITYKEY");

        cityOffersRecycler = view.findViewById(R.id.recycler_view_city_offers);

        getActivity().getLoaderManager().initLoader(Common.CITY_OFFERS_PRICE_LOADER_ID, null, this);

        return view;
    }

    @Override
    public Loader<List<CityOffer>> onCreateLoader(int id, Bundle args) {
        return new CityOffersLoader(getContext(), Common.getCityOffersById(cityId));
    }

    @Override
    public void onLoadFinished(Loader<List<CityOffer>> loader, List<CityOffer> data) {
        if(data != null && !data.isEmpty()){
            cityOffersRecycler.setHasFixedSize(true);
            cityOffersRecycler.setItemViewCacheSize(20);
            cityOffersRecycler.setDrawingCacheEnabled(true);
            cityOffersRecycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            cityOffersRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            cityOffersRecycler.setItemAnimator(new DefaultItemAnimator());
            cityOffersAdapter = new CityOffersAdapter(data);
            cityOffersRecycler.setAdapter(cityOffersAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<CityOffer>> loader) {

    }
}
