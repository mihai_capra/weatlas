package com.example.mihai.weatlas.fragments;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mihai.weatlas.R;
import com.example.mihai.weatlas.adapters.CityAdapter;
import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.loaders.CityLoader;
import com.example.mihai.weatlas.models.City;

import java.util.ArrayList;
import java.util.List;

public class CityFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<List<City>>{

    private RecyclerView recyclerView;
    private CityAdapter cityAdapter;
    private int countryId;
    private List<City> itemsFound;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.city_fragment, container, false);
        countryId = getActivity().getIntent().getIntExtra(Common.COUNTRY_ID, 0);
        recyclerView = view.findViewById(R.id.recycler_view_city_fragment);

        getActivity().getLoaderManager().initLoader(Common.CITY_OFFERS_LOADER_ID, null, this);

        return view;
    }

    @Override
    public Loader<List<City>> onCreateLoader(int id, Bundle args) {
        return new CityLoader(getContext(), Common.getCitiesOffersWithCountryId(countryId));
    }

    @Override
    public void onLoadFinished(Loader<List<City>> loader, List<City> cities) {
        if(cities != null && !cities.isEmpty()){
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(20);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            cityAdapter = new CityAdapter(cities);
            recyclerView.setAdapter(cityAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<City>> loader) {

    }

    public void findCity(String name){

        int size = recyclerView.getAdapter().getItemCount();

        itemsFound = new ArrayList<>();

        for(int i=0; i<size; i++){
            City item = ((CityAdapter)recyclerView.getAdapter()).getItem(i);
            if(item.getName().contains(name)
                    || item.getName().startsWith(name)){
                itemsFound.add(item);
            }
            else if(item.getIATACity().contains(name) || item.getIATACity().equals(name)){
                itemsFound.add(item);
            }
            else{
                Toast.makeText(getContext(), "Nothing Found!", Toast.LENGTH_SHORT).show();
            }
        }
        recyclerView.setAdapter(new CityAdapter(itemsFound));
    }
}
