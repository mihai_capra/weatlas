package com.example.mihai.weatlas.activities;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.loaders.GetFromLoader;
import com.example.mihai.weatlas.models.GetFrom;
import com.example.mihai.weatlas.models.GetTo;
import com.example.mihai.weatlas.network.NetworkUtil;
import com.example.mihai.weatlas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CreateTransfer extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<GetFrom>> {

    private ArrayAdapter<GetFrom> getFromArrayAdapter;
    private ArrayAdapter<GetTo> getToArrayAdapter;
    private Spinner spinner_from;
    private Spinner spinner_to;

    private int IDCity;
    private int fromIdInt;
    private int toIdInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_transfer);

        IDCity = getIntent().getIntExtra("CITY_TRANSFER_ID", 0);
        spinner_from = (Spinner) findViewById(R.id.spinner_from);
        spinner_to = (Spinner) findViewById(R.id.spinner_to);

        getLoaderManager().initLoader(Common.GET_FROM_LOADER_ID, null, this);

        spinner_from.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                GetFrom getFrom = (GetFrom) adapterView.getItemAtPosition(i);
                String fromId = Common.TRANSFER_TO +
                        "&IDCity=" + IDCity + "&placeId=" + getFrom.getId();
                new GetToList().execute(fromId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        Button button = (Button)findViewById(R.id.btn_search_transfer);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetFrom getFrom = (GetFrom) spinner_from.getSelectedItem();
                GetTo getTo = (GetTo) spinner_to.getSelectedItem();

                Intent intent = new Intent(view.getContext(), CarActivity.class);
                intent.putExtra("GET_FROM_ID", getFrom.getId());
                intent.putExtra("GET_TO_ID", getTo.getId());
                startActivity(intent);
            }
        });
    }

    @Override
    public Loader<List<GetFrom>> onCreateLoader(int i, Bundle bundle) {
        String id = Common.TRANSFER_FROM + "&IDCity=" + IDCity;
        return new GetFromLoader(this , id);

    }

    @Override
    public void onLoadFinished(Loader<List<GetFrom>> loader, List<GetFrom> getFromList) {
        if(getFromList != null && !getFromList.isEmpty()){
            getFromArrayAdapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_item, getFromList);

            getFromArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            spinner_from.setAdapter(getFromArrayAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<GetFrom>> loader) {
        getFromArrayAdapter.clear();
    }


    private class GetToList extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            URL url = NetworkUtil.createURL(strings[0]);

            try{
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(httpURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while((line = bufferedReader.readLine()) != null){
                    sb.append(line);
                }
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            List<GetTo> items = new ArrayList<>();
            if(s!=null){
                try {
                    JSONObject baseJSON = new JSONObject(s);
                    JSONArray values = baseJSON.getJSONArray("place");

                    for(int i = 0; i < values.length(); i++){
                        JSONObject jsonItem = values.getJSONObject(i);
                        items.add(new GetTo(
                                jsonItem.getInt("toId"),
                                jsonItem.getString("value"),
                                jsonItem.getString("fromId")
                        ));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if(!items.isEmpty()){
                getToArrayAdapter = new ArrayAdapter<>(getBaseContext(),
                        android.R.layout.simple_spinner_item, items);

                getFromArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                spinner_to.setAdapter(getToArrayAdapter);
            }


        }
    }


}
