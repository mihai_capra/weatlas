package com.example.mihai.weatlas.models;

/**
 * Created by mihai on 9/14/2017.
 */

public class GetFrom {

    private int id;
    private String value;

    public GetFrom(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
