package com.example.mihai.weatlas.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.mihai.weatlas.models.GetFrom;
import com.example.mihai.weatlas.network.GetFromList;

import java.util.List;

/**
 * Created by mihai on 9/14/2017.
 */

public class GetFromLoader extends AsyncTaskLoader<List<GetFrom>> {

    private String stringURL;

    public GetFromLoader(Context context, String stringURL) {
        super(context);
        this.stringURL = stringURL;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<GetFrom> loadInBackground() {
        if(stringURL == null && stringURL.length() < 1){
            return null;
        }

        return GetFromList.FetchGetFromList(stringURL);
    }
}
