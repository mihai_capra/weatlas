package com.example.mihai.weatlas.adapters;

import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mihai.weatlas.models.Car;
import com.example.mihai.weatlas.R;

import java.util.List;

/**
 * Created by mihai on 9/13/2017.
 */

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private List<Car> cars;

    public CarAdapter(List<Car> cars){
        this.cars = cars;
    }

    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_car, parent, false);
        return new CarAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CarAdapter.ViewHolder holder, int position) {
        holder.carImage.setImageURI(Uri.parse(cars.get(position).getPhotoLink()));
        holder.carBrand.setText(cars.get(position).getCarBrand());
        holder.tripType.setText(cars.get(position).getTripType());
        holder.rentPrice.setText(cars.get(position).getPriceTrip());
        holder.maxPersons.setText(cars.get(position).getMaxPersons());
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardItem;
        TextView carBrand;
        TextView tripType;
        TextView rentPrice;
        ImageView carImage;
        TextView maxPersons;

        public ViewHolder(View itemView) {
            super(itemView);

            cardItem = (CardView)itemView.findViewById(R.id.card_view_car);
            carImage = (ImageView)itemView.findViewById(R.id.car_image);
            carBrand = (TextView)itemView.findViewById(R.id.car_brand);
            tripType = (TextView)itemView.findViewById(R.id.class_of_trip);
            rentPrice = (TextView)itemView.findViewById(R.id.rent_car_price);
            maxPersons = itemView.findViewById(R.id.max_number_of_persons);
        }

    }
}
