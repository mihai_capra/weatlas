package com.example.mihai.weatlas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.fragments.FragmentAdapter;
import com.example.mihai.weatlas.fragments.TripsFragment;
import com.example.mihai.weatlas.loaders.TripsLoader;


public class MainActivity extends AppCompatActivity {
    private Fragment currentFragment;
    private FragmentManager fm;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager)findViewById(R.id.view_pager_id);
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager(), this));
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout_id);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_search:

                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Item Search");
                final EditText input = new EditText(this);
                alert.setView(input);
                //Швейцария
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        TripsFragment page = (TripsFragment) getSupportFragmentManager().
                                findFragmentByTag("android:switcher:" + R.id.view_pager_id + ":"
                                + viewPager.getCurrentItem());

                        if(viewPager.getCurrentItem() == 0 && page != null){
                            page.findCountry(input.getText().toString());
                        }
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        return;
                    }
                });
                alert.show();
            case R.id.action_settings:
                String link = Common.COUNTRY_LIST_OFFERS + "&Order=ActivitiesCount";
                new TripsLoader(this, link);
        }
        return super.onOptionsItemSelected(item);
    }
}
