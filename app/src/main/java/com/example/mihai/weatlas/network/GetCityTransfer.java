package com.example.mihai.weatlas.network;

import android.text.TextUtils;

import com.example.mihai.weatlas.models.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetCityTransfer {

    public static List<Item> parseJsonToTransferList(String response) {
        if (TextUtils.isEmpty(response)) {
            return null;
        }
        List<Item> values = new ArrayList<>();
        try {
            JSONObject baseJSON = new JSONObject(response);
            JSONArray jsonArray = baseJSON.getJSONArray("city");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                values.add(new Item(jsonItem.getInt("id"), jsonItem.getString("nameRu")));
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return values;
    }


    public static List<Item> getCountryTransferList(String stringURL){
        URL url = NetworkUtil.createURL(stringURL);

        String resultJSON = null;

        try{
            resultJSON = NetworkUtil.makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return parseJsonToTransferList(resultJSON);
    }
}
