package com.example.mihai.weatlas.adapters;


import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mihai.weatlas.activities.DetailsActivity;
import com.example.mihai.weatlas.models.CityOffer;
import com.example.mihai.weatlas.R;

import java.util.List;

public class CityOffersAdapter extends
        RecyclerView.Adapter<CityOffersAdapter.ViewHolder>{
    private List<CityOffer> items;

    public CityOffersAdapter(List<CityOffer> items){
        this.items = items;
    }

    @Override
    public CityOffersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_offer, parent, false);
        return new CityOffersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.offerImage.setImageURI(Uri.parse(items.get(position).getImageLink()));
        holder.offerName.setText(items.get(position).getOfferName());
        holder.goTypePlusTime.setText(items.get(position).getGoType());
        holder.offerPrice.setText(items.get(position).getOfferPrice());
        holder.offerPriceFor.setText(items.get(position).getOfferPriceFor());
        holder.rating.setText(items.get(position).getRating());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DetailsActivity.class);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView offerImage;
        TextView offerName;
        TextView goTypePlusTime;
        TextView offerPrice;
        TextView offerPriceFor;
        TextView rating;

        public ViewHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.card_view_offer);
            offerImage = itemView.findViewById(R.id.city_offer_image);
            offerName = itemView.findViewById(R.id.city_offer_name);
            goTypePlusTime = itemView.findViewById(R.id.go_type_offer_time);
            offerPrice = itemView.findViewById(R.id.price_offer);
            offerPriceFor = itemView.findViewById(R.id.offer_price_for);
            rating = itemView.findViewById(R.id.rate_offer);
        }
    }
}
