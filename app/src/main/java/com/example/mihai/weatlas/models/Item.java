package com.example.mihai.weatlas.models;

public class Item {

    private int id;
    private String countryName;

    public Item(int id, String countryName) {
        this.id = id;
        this.countryName = countryName;
    }

    public int getId() {
        return id;
    }

    public String getCountryName() {
        return countryName;
    }
}
