package com.example.mihai.weatlas.network;


import android.text.TextUtils;

import com.example.mihai.weatlas.models.Car;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetCarOffers {

    public static List<Car> getJSONList(String response){

        if(TextUtils.isEmpty(response)){
            return null;
        }

        List<Car> items = new ArrayList<>();

        try{
            JSONObject baseJSON = new JSONObject(response);
            JSONArray values = baseJSON.getJSONArray("action");
            for(int i = 0; i < values.length(); i++){
                JSONObject jsonItem = values.getJSONObject(i);

                String class_car = jsonItem.getString("class");
                String car_brand = jsonItem.getString("description");

                String price = jsonItem.getString("price");
                price += " " + jsonItem.getString("currency");


                String photoLink = jsonItem.getString("photo");

                String maxPersons = "до " + jsonItem.getString("maxPersons") + "-x пассажиров";

                items.add(new Car(
                        car_brand,
                        class_car,
                        price,
                        photoLink,
                        maxPersons
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return items;
    }

    public static List<Car> getCarList(String stringURL){
        URL url = NetworkUtil.createURL(stringURL);

        String resultJSON = null;
        try{
            resultJSON = NetworkUtil.makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return getJSONList(resultJSON);
    }
}
