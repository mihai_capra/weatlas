package com.example.mihai.weatlas.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

import java.io.UnsupportedEncodingException;


public class StringUtil {

    public static String ConvertToRussian(String value) throws UnsupportedEncodingException {
        byte bytes[] = value.getBytes();
        return new String(bytes, "UTF-8");
    }

}
