package com.example.mihai.weatlas.activities;

import android.app.LoaderManager;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.mihai.weatlas.adapters.CarAdapter;
import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.loaders.CarLoader;
import com.example.mihai.weatlas.models.Car;
import com.example.mihai.weatlas.R;

import java.util.List;


public class CarActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<Car>>{

    private int getFromId;
    private int getToId;

    private RecyclerView recyclerView;
    private CarAdapter carAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);

        getFromId = getIntent().getIntExtra("GET_FROM_ID", 0);
        getToId = getIntent().getIntExtra("GET_TO_ID", 0);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_car);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(8, null, this);
    }


    @Override
    public Loader<List<Car>> onCreateLoader(int i, Bundle bundle) {
        String link = Common.CAR_OFFERS + "&fromId=" + getFromId +
                "&toId=" + getToId;
        return new CarLoader(this, link);
    }

    @Override
    public void onLoadFinished(Loader<List<Car>> loader, List<Car> cars) {
        if(cars != null && !cars.isEmpty()){
            carAdapter = new CarAdapter(cars);
            recyclerView.setAdapter(carAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Car>> loader) {

    }
}
