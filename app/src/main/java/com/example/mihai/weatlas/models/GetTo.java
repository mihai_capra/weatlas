package com.example.mihai.weatlas.models;


public class GetTo {
    private int id;
    private String value;
    private String fromId;

    public GetTo(int id, String value, String fromId) {
        this.id = id;
        this.value = value;
        this.fromId = fromId;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getFromId() {
        return fromId;
    }

    @Override
    public String toString() {
        return value;
    }
}
