package com.example.mihai.weatlas.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.mihai.weatlas.models.City;
import com.example.mihai.weatlas.network.GetCitiesFromCountry;

import java.util.List;

public class CityLoader extends AsyncTaskLoader<List<City>>{

    private String stringURL;

    public CityLoader(Context context, String stringURL) {
        super(context);
        this.stringURL = stringURL;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<City> loadInBackground() {
        return GetCitiesFromCountry.getCitiesOffersList(stringURL);
    }
}
