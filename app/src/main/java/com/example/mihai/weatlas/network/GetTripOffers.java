package com.example.mihai.weatlas.network;

import android.text.TextUtils;

import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.models.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;



public class GetTripOffers {

    public static List<Country> getJSONList(String response){

        if(TextUtils.isEmpty(response)){
            return null;
        }

        List<Country> values = new ArrayList<>();

        try{
            JSONObject baseJSON = new JSONObject(response);
            JSONArray jsonArray = baseJSON.getJSONArray("country");

            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonItem = jsonArray.getJSONObject(i);

                String imageURl;

                if(TextUtils.isEmpty(jsonItem.getString("img960x876"))){
                    imageURl = Common.BASE_IMAGE_URL;
                }
                else{
                    imageURl = jsonItem.getString("img960x876");
                }

                String activities = (Integer.parseInt(jsonItem.getString("activites")) > 1)?
                        jsonItem.getString("activites") + " экскурсии" :
                        jsonItem.getString("activites") + " экскурсия" ;

                values.add(new Country(
                        jsonItem.getInt("id"),
                        jsonItem.getString("nameRu"),
                        activities,
                        imageURl,
                        jsonItem.getString("IATACountry")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return values;
    }

    public static List<Country> getCountryList(String stringURL){
        URL url = NetworkUtil.createURL(stringURL);

        String resultJSON = null;
        try{
            resultJSON = NetworkUtil.makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Fetch data from server
        return getJSONList(resultJSON);
    }
}
