package com.example.mihai.weatlas.loaders;


import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.mihai.weatlas.models.CityOffer;
import com.example.mihai.weatlas.network.GetCityOffers;

import java.util.List;

public class CityOffersLoader extends AsyncTaskLoader<List<CityOffer>>{
    private String stringURL;

    public CityOffersLoader(Context context, String stringURL) {
        super(context);
        this.stringURL = stringURL;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<CityOffer> loadInBackground() {
        return GetCityOffers.getCityOffersList(stringURL);
    }
}
