package com.example.mihai.weatlas.adapters;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mihai.weatlas.fragments.CityOffersFragment;
import com.example.mihai.weatlas.models.City;
import com.example.mihai.weatlas.R;
import java.util.List;


public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {
    private List<City> items;

    public CityAdapter(List<City> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.cityImage.setImageURI(Uri.parse(items.get(position).getImageLink()));
        holder.cityName.setText(items.get(position).getName());
        holder.cityOffers.setText(items.get(position).getNumberOfActivities());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CityOffersFragment cityOffersFragment = new CityOffersFragment();

                Bundle bundle = new Bundle();
                bundle.putInt("CITYKEY", items.get(holder.getAdapterPosition()).getId());
                cityOffersFragment.setArguments(bundle);

                FragmentManager fragmentManager =
                        ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                        .replace(R.id.city_fragment_id,
                                cityOffersFragment, "CityOffersFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView cityImage;
        TextView cityName;
        TextView cityOffers;

        public ViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.card_view_item);
            cityImage = (ImageView) itemView.findViewById(R.id.image_view_item);
            cityName = (TextView) itemView.findViewById(R.id.name_txt_item);
            cityOffers = (TextView)itemView.findViewById(R.id.offers_number_txt);
        }
    }

    public City getItem(int position){
        return items.get(position);
    }
}
