package com.example.mihai.weatlas.adapters;


import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.mihai.weatlas.activities.CreateTransfer;
import com.example.mihai.weatlas.models.Item;
import com.example.mihai.weatlas.R;

import java.util.List;

public class CityTransferAdapter extends RecyclerView.Adapter<CityTransferAdapter.ViewHolder>{

    List<Item> items;

    public CityTransferAdapter(List<Item> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transfer_card, parent, false);
        return new CityTransferAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.name.setText(items.get(position).getCountryName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CreateTransfer.class);
                intent.putExtra("CITY_TRANSFER_ID", items.get(position).getId());
                view.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.card_transfer_id);
            name = itemView.findViewById(R.id.country_transfer_name);
        }
    }
}