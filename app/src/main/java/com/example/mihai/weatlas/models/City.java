package com.example.mihai.weatlas.models;

public class City {

    private int id;
    private String name;
    private String numberOfActivities;
    private String imageLink;
    private String IATACity;

    public City(int id, String name, String numberOfActivities, String imageLink, String IATACity) {
        this.id = id;
        this.name = name;
        this.numberOfActivities = numberOfActivities;
        this.imageLink = imageLink;
        this.IATACity = IATACity;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumberOfActivities() {
        return numberOfActivities;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getIATACity() {
        return IATACity;
    }
}
