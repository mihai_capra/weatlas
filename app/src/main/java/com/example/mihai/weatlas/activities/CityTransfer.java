package com.example.mihai.weatlas.activities;

import android.app.LoaderManager;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.mihai.weatlas.adapters.CityTransferAdapter;
import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.loaders.CityTransferLoader;
import com.example.mihai.weatlas.models.Item;
import com.example.mihai.weatlas.R;

import java.util.List;

public class CityTransfer extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Item>>{

    private RecyclerView recyclerView;
    private CityTransferAdapter transferAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_transfer);


        recyclerView = (RecyclerView)findViewById(R.id.city_transfer);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(7, null, this);
    }

    @Override
    public Loader<List<Item>> onCreateLoader(int i, Bundle bundle) {
        String cityID = Common.CITY_TRANSFER + "&IDCountry=" + getIntent().getIntExtra("CITY_ID", 0);
        return new CityTransferLoader(this, cityID);
    }

    @Override
    public void onLoadFinished(Loader<List<Item>> loader, List<Item> itemList) {
        if(itemList != null && !itemList.isEmpty()){
            transferAdapter = new CityTransferAdapter(itemList);
            recyclerView.setAdapter(transferAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Item>> loader) {

    }
}
