package com.example.mihai.weatlas.loaders;


import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.mihai.weatlas.models.Car;
import com.example.mihai.weatlas.network.GetCarOffers;

import java.util.List;

public class CarLoader extends AsyncTaskLoader<List<Car>>{

    String stringURL;

    public CarLoader(Context context, String stringURL) {
        super(context);
        this.stringURL = stringURL;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<Car> loadInBackground() {
        return GetCarOffers.getCarList(stringURL);
    }
}
