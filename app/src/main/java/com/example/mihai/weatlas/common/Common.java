package com.example.mihai.weatlas.common;


public class Common {

    public static final String AID = "13541";
    public static final String API_KEY = "78316ba3fe8796011f54ea0dbd657556";
    public static final String BASE_URL = "http://api.weatlas.com/?=";
    public static final String LANG_API_RU = "RU";

    //default image
    public static final String BASE_IMAGE_URL = "https://images.pexels.com/photos/163194/" +
            "old-retro-antique-vintage-163194.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb";

    //Transfers

    public static final String COUNTRY_TRANSFER = "http://api.weatlas.com/transfers/countrylist/?" +
            "aid=" + AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU + "&mode=JSON";

    public static final String CITY_TRANSFER = "http://api.weatlas.com/transfers/citylist/?aid=" +
            AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU + "&mode=JSON";

    //from
    public static final String TRANSFER_FROM = "http://api.weatlas.com/transfers/getfrom/?aid=" +
            AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU + "&mode=JSON";

    //to
    public static final String TRANSFER_TO = "http://api.weatlas.com/transfers/getto/?aid=" +
            AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU + "&mode=JSON";

    //Trip Offers
    public static final String COUNTRY_LIST_OFFERS = "http://api.weatlas.com/export/countrylist/?" +
            "aid=" + AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU +
            "&mode=JSON&activityType=Excursion";

    public static final String CITY_LIST_OFFERS = "http://api.weatlas.com/export/citylist/?" +
            "aid=" + AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU +
            "&mode=JSON"+"&OnlyWithActivities=true";


    public static final String CITY_OFFERS = "http://api.weatlas.com/export/activitylist/?" +
            "aid=" + AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU +
            "&mode=JSON";


    public static final String CAR_OFFERS = "http://api.weatlas.com/transfers/getprice/?" +
            "aid=" + AID + "&key=" + API_KEY + "&Lang=" + LANG_API_RU +
            "&mode=JSON";

    //Loaders ID's
    public static final int GET_FROM_LOADER_ID = 1;
    public static final int TRIP_OFFERS_LOADER_ID = 2;
    public static final int CITY_OFFERS_LOADER_ID = 3;
    public static final int CITY_OFFERS_PRICE_LOADER_ID = 4;


    public static String getCitiesOffersWithCountryId(int id){
        return CITY_LIST_OFFERS + "&IDCountry=" + id;
    }

    public static String getCityOffersById(int id){
        return CITY_OFFERS + "&IDCity=" + id;
    }

    //Intents
    public final static String COUNTRY_ID = "COUNTRY_ID";
    public final static String CITY_ID = "CITY_ID";
}
