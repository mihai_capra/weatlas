package com.example.mihai.weatlas.network;


import android.text.TextUtils;

import com.example.mihai.weatlas.common.Common;
import com.example.mihai.weatlas.models.City;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetCitiesFromCountry {

    public static List<City> getJSONList(String response){

        if(TextUtils.isEmpty(response)){
            return null;
        }

        List<City> items = new ArrayList<>();

        try{
            JSONObject baseJSON = new JSONObject(response);
            JSONArray values = baseJSON.getJSONArray("city");
            for(int i = 0; i < values.length(); i++){
                JSONObject jsonItem = values.getJSONObject(i);
                String imageLink = (TextUtils.isEmpty(jsonItem.getString("img960x876"))) ?
                        Common.BASE_IMAGE_URL : jsonItem.getString("img960x876");

                String activities = (Integer.parseInt(jsonItem.getString("activites")) > 1)?
                        jsonItem.getString("activites") + " экскурсии" :
                        jsonItem.getString("activites") + " экскурсия" ;

                items.add(new City(
                        jsonItem.getInt("id"),
                        jsonItem.getString("nameRu"),
                        activities,
                        imageLink,
                        jsonItem.getString("IATACity")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return items;
    }

    public static List<City> getCitiesOffersList(String stringURL){
        URL url = NetworkUtil.createURL(stringURL);

        String resultJSON = null;
        try{
            resultJSON = NetworkUtil.makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return getJSONList(resultJSON);
    }
}
