package com.example.mihai.weatlas.models;



public class Car {

    private String carBrand;
    private String tripType;
    private String priceTrip;
    private String photoLink;
    private String maxPersons;


    public Car(String carBrand, String tripType, String priceTrip, String photoLink, String maxPersons) {
        this.carBrand = carBrand;
        this.tripType = tripType;
        this.priceTrip = priceTrip;
        this.photoLink = photoLink;
        this.maxPersons = maxPersons;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String getTripType() {
        return tripType;
    }

    public String getPriceTrip() {
        return priceTrip;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public String getMaxPersons() {
        return maxPersons;
    }
}
